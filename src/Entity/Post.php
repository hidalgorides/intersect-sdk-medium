<?php

namespace Intersect\SDK\Medium\Entity;

use Intersect\SDK\Medium\Entity\PublishStatusType;

class Post {

    private $authorId;
    private $canonicalUrl;
    private $contentFormat = 'html';
    private $content;
    private $id;
    private $license = 'all-rights-reserved';
    private $licenseUrl;
    private $notifyFollowers = true;
    private $publishedAt;
    private $publishStatus = PublishStatusType::PUBLIC;
    private $tags = [];
    private $title;
    private $url;

    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    public function getCanonicalUrl()
    {
        return $this->canonicalUrl;
    }

    public function setCanonicalUrl($canonicalUrl)
    {
        $this->canonicalUrl = $canonicalUrl;
    }

    public function getContentFormat()
    {
        return $this->contentFormat;
    }

    public function setContentFormat($contentFormat)
    {
        $this->contentFormat = $contentFormat;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getLicense()
    {
        return $this->license;
    }

    public function setLicense($license)
    {
        $this->license = $license;
    }

    public function getLicenseUrl()
    {
        return $this->licenseUrl;
    }

    public function setLicenseUrl($licenseUrl)
    {
        $this->licenseUrl = $licenseUrl;
    }

    public function getNotifyFollowers()
    {
        return $this->notifyFollowers;
    }

    public function setNotifyFollowers(bool $notifyFollowers)
    {
        $this->notifyFollowers = $notifyFollowers;
    }

    public function getPublishStatus()
    {
        return $this->publishStatus;
    }

    public function setPublishStatus($publishStatus)
    {
        $this->publishStatus = $publishStatus;
    }

    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

}
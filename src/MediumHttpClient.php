<?php

namespace Intersect\SDK\Medium;

use Intersect\SDK\Core\HttpClient;
use Intersect\SDK\Core\HttpRequestData;

class MediumHttpClient extends HttpClient {

    private static $AUTHORIZE_URL_ENDPOINT = 'https://medium.com/m/oauth/authorize';
    private static $API_BASE_ENDPOINT = 'https://api.medium.com/v1';

    protected $scopes = ['listPublications', 'basicProfile', 'publishPost'];

    protected function getBaseAuthorizationEndpoint()
    {
        return self::$AUTHORIZE_URL_ENDPOINT;
    }

    /**
     * @param $userId
     * @param array $postData
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function createPost($userId, array $postData)
    {
        $headers = [
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setFormData($postData);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/users/' . $userId . '/posts', $requestData);

        return json_decode((string)$response->getBody());
    }

    /**
     * @param $publicationId
     * @param array $postData
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function createPostForPublication($publicationId, array $postData)
    {
        $headers = [
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setFormData($postData);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/publications/' . $publicationId . '/posts', $requestData);

        return json_decode((string)$response->getBody());
    }

    /**
     * @param $code
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getAccessTokenDetails($code)
    {
        $payload = [
            'code' => $code,
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->getRedirectUri(),
        ];

        $requestData = new HttpRequestData();
        $requestData->setFormData($payload);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/tokens', $requestData);

        return json_decode((string) $response->getBody());
    }

    /**
     * @param $publicationId
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getContributors($publicationId)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);

        $response = $this->request('GET', self::$API_BASE_ENDPOINT . '/publications/' . $publicationId . '/contributors', $requestData);

        return json_decode((string)$response->getBody());
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getPublications($userId)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);

        $response = $this->request('GET', self::$API_BASE_ENDPOINT . '/users/' . $userId . '/publications', $requestData);

        return json_decode((string)$response->getBody());
    }

    /**
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function getUserDetails()
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8',
            'Authorization' => 'Bearer ' . $this->getAccessToken()
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);

        $response = $this->request('GET', self::$API_BASE_ENDPOINT . '/me', $requestData);

        return json_decode((string)$response->getBody());

    }

    /**
     * @param $refreshToken
     * @return mixed
     * @throws \Intersect\SDK\HttpClientException
     */
    public function refreshAccessToken($refreshToken)
    {
        $data = [
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token',
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Accept-Charset' => 'utf-8'
        ];

        $requestData = new HttpRequestData();
        $requestData->setHeaders($headers);
        $requestData->setFormData($data);

        $response = $this->request('POST', self::$API_BASE_ENDPOINT . '/tokens', $requestData);

        return json_decode((string) $response->getBody());
    }

}